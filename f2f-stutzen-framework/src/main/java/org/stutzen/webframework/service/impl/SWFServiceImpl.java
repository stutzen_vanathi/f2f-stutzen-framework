package org.stutzen.webframework.service.impl;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;

import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;
import org.stutzen.webframework.logger.LogFormater;
import org.stutzen.webframework.logger.LoggerManager;
import org.stutzen.webframework.service.SWFService;
import org.stutzen.webframework.util.SWFUtil;


@Service
public class SWFServiceImpl<T>
implements SWFService<T>
{

	@Autowired
	private SqlSessionFactory sf;
	private Class<T> type;
	public static final String SUFFIX_SELECT_QUERY = "selectByExample";
	public static final String SUFFIX_SELECT_PRIMARY_QUERY = "selectByPrimaryKey";
	public static final String SUFFIX_INSERT_QUERY = "insert";
	public static final String SUFFIX_UPDATE_QUERY = "updateByPrimaryKeySelective";
	public static final String SUFFIX_DELETE_QUERY = "deleteByPrimaryKey";

	public SWFServiceImpl()
	{
		this.type = getGenericType();
	}

	protected SqlSessionFactory getSessionFactory()
	{
		return this.sf;
	}

	public T get(Integer id) throws PersistenceException
	{
		SqlSession session = this.sf.openSession();
		T obj = null;
		try
		{
			String query = this.type.getCanonicalName().replace(".entity.", ".mapper.") + "Mapper" + "." + "selectByPrimaryKey";
			LoggerManager.info(LogFormater.getLogMessage(getClass(), "get", "Mapper Query : " + query));
			obj = session.selectOne(query, id);
		}
		finally
		{
			session.close();
		}
		return obj;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<T> getAll() throws PersistenceException
	{
		SqlSession session = this.sf.openSession();
		ArrayList<T> list = null;
		try
		{
			String query = this.type.getCanonicalName().replace(".entity.", ".mapper.") + "Mapper" + "." + "selectByExample";
			LoggerManager.info(LogFormater.getLogMessage(getClass(), "getAll", "Mapper Query : " + query));
			list = (ArrayList<T>)session.selectList(query);
		}
		finally
		{
			session.close();
		}
		return list;
	}

	public T create(T o) throws PersistenceException {
		SqlSession session = this.sf.openSession();
		try
		{
			Field fl = ReflectionUtils.findField(o.getClass(), "createdon");
			if (!SWFUtil.isNz(fl)) {
				ReflectionUtils.makeAccessible(fl);
				ReflectionUtils.setField(fl, o, new Date());
			}
			fl = ReflectionUtils.findField(o.getClass(), "isactive");
			if (!SWFUtil.isNz(fl)) {
				ReflectionUtils.makeAccessible(fl);
				ReflectionUtils.setField(fl, o, Byte.valueOf((byte)1));
			}
			fl = ReflectionUtils.findField(o.getClass(), "password");
			if (!SWFUtil.isNz(fl)) {
				ReflectionUtils.makeAccessible(fl);
				String encrpt = new BCryptPasswordEncoder().encode((String)ReflectionUtils.getField(fl, o));
				ReflectionUtils.setField(fl, o, String.valueOf(encrpt));
			}

			String query = this.type.getCanonicalName().replace(".entity.", ".mapper.") + "Mapper" + "." + "insert";
			LoggerManager.info(LogFormater.getLogMessage(getClass(), "create", "Mapper Query : " + query));
			session.insert(query, o);
			session.commit();
		}
		finally
		{
			session.close();
		}
		return o;
	}

	public int update(T o) throws PersistenceException
	{
		SqlSession session = this.sf.openSession();
		Integer status = null;
		try
		{
			Field fl = ReflectionUtils.findField(o.getClass(), "lastmodifiedon");
			if (!SWFUtil.isNz(fl)) {
				ReflectionUtils.makeAccessible(fl);
				ReflectionUtils.setField(fl, o, new Date());
			}
			String query = this.type.getCanonicalName().replace(".entity.", ".mapper.") + "Mapper" + "." + "updateByPrimaryKeySelective";
			LoggerManager.info(LogFormater.getLogMessage(getClass(), "update", "Mapper Query : " + query));
			status = Integer.valueOf(session.update(query, o));
			session.commit();
		}
		finally
		{
			session.close();
		}
		return status.intValue();
	}

	public int delete(Integer id) throws PersistenceException
	{
		SqlSession session = this.sf.openSession();
		Integer status = null;
		try
		{
			String query = this.type.getCanonicalName().replace(".entity.", ".mapper.") + "Mapper" + "." + "deleteByPrimaryKey";
			LoggerManager.info(LogFormater.getLogMessage(getClass(), "delete", "Mapper Query : " + query));
			status = Integer.valueOf(session.delete(query, id));
			session.commit();
		}
		finally
		{
			session.close();
		}
		return status.intValue();
	}

	@SuppressWarnings("unchecked")
	private Class<T> getGenericType()
	{
		return (Class<T>) GenericTypeResolver.resolveTypeArgument(getClass(), SWFService.class);
	}
}