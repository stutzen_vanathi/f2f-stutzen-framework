package org.stutzen.webframework.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.stutzen.webframework.logger.LogFormater;
import org.stutzen.webframework.logger.LoggerManager;
import org.stutzen.webframework.util.SWFUtil;

public class SWFAuthenticationEntryPoint
implements AuthenticationEntryPoint
{

	@Value("${security.login.url:/login}")
	String loginurl;

	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
			throws IOException, ServletException
	{
		LoggerManager.info(LogFormater.getLogMessage(getClass(), "commence", "Enter into swwf auth entry point"));
		if (SWFUtil.isAjax(request)) {
			LoggerManager.info(LogFormater.getLogMessage(getClass(), "commence", "Received request is ajax so swf framwork will trying to send 401-SC_UNAUTHORIZED error"));
			request.getSession().removeAttribute("SPRING_SECURITY_SAVED_REQUEST");
			LoggerManager.info(LogFormater.getLogMessage(getClass(), "commence", " contentType : " + request.getContentType()));
			response.sendError(401, "Unauthorized");
		} else {
			response.sendRedirect(request.getContextPath() + this.loginurl);
		}
	}
}