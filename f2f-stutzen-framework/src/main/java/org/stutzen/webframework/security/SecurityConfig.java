package org.stutzen.webframework.security;

import javax.servlet.Filter;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig
{
	@Autowired
	protected AuthenticationManager authenticationManager;
	@Configuration
	@Order(1)
	public static class ApiWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter
	{

		@Autowired
		private Environment env;

		protected void configure(HttpSecurity http) throws Exception
		{
			http.antMatcher(this.env.getProperty("security.api.url", "/api") + "/**").authorizeRequests()
			.anyRequest().authenticated()
			.and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
			.csrf().disable()
			.exceptionHandling()
			.authenticationEntryPoint(new SWFRestAuthenticationEntryPoint())
			.and()
			.addFilterBefore(getTokenFilter(), UsernamePasswordAuthenticationFilter.class);
		}

		@Bean
		public Filter getTokenFilter()
		{
			Filter up=new UsernamePasswordAuthenticationFilter();
			return new SWFTokenAuthenticationFilter(new AntPathRequestMatcher(this.env.getProperty("security.api.url", "/api") + "/**"));
		}
	}
	@Configuration
	public static class FormLoginWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

		@Autowired
		DataSource dataSource;

		@Autowired
		private Environment env;

		@Autowired
		public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
			auth.jdbcAuthentication().dataSource(this.dataSource)
			.passwordEncoder(new BCryptPasswordEncoder())
			.usersByUsernameQuery(
					"select username,password, isactive from tbl_user where username=?")
					.authoritiesByUsernameQuery(
							"select username, rolename from tbl_user_role where username=?");
		}

		public void configure(WebSecurity web) throws Exception
		{
			web.ignoring().antMatchers(new String[] { "/resources/**", "/js/**", "/css/**", "/images/**", "/favicon.ico", "/error/**" });

			String ignorePatters = this.env.getProperty("security.url.ignore", "");
			if(ignorePatters.length() > 0){
				web.ignoring().antMatchers(ignorePatters.split(","));
			}
		}

		protected void configure(HttpSecurity http) throws Exception
		{
			http.authorizeRequests()
			.antMatchers(new String[] {
					this.env.getProperty("security.login.url", "/login") }).permitAll()
					.anyRequest().authenticated()
					.and()
					.formLogin()
					.loginPage(this.env.getProperty("security.login.url", "/login"))
					.failureUrl(this.env.getProperty("security.loginfailed.url", "/login?error"))
					.loginProcessingUrl("/swf/login_check")
					.usernameParameter("username").passwordParameter("password")
					.successHandler(authSuccess())
					.failureHandler(authFailure())
					.and()
					.logout()
					.logoutSuccessUrl(this.env.getProperty("security.login.url", "/index"))
					.logoutUrl("/swf/logout")
					.deleteCookies(new String[] {"JSESSIONID" }).deleteCookies(new String[] { "SWF_REMEMBERME" })
					.deleteCookies(new String[] { "f2f-auth-key" })
					.and()
					.rememberMe()
					.tokenRepository(persistentTokenRepository())
					.tokenValiditySeconds(((Integer)this.env.getProperty("security.rememberme.validity", Integer.class, Integer.valueOf(86400))).intValue())
					.and()
					.exceptionHandling()
					.authenticationEntryPoint(authPoint())
					.and()
					.csrf().disable();
		}

		@Bean
		public PersistentTokenRepository persistentTokenRepository()
		{
			JdbcTokenRepositoryImpl db = new JdbcTokenRepositoryImpl();
			db.setDataSource(this.dataSource);
			return db;
		}

		 @Override
		    @Bean(name = "authenticationManager")
		    public AuthenticationManager authenticationManagerBean() throws Exception {
		        return super.authenticationManagerBean();
		    }

		@Bean
		public AuthenticationEntryPoint authPoint()
		{
			return new SWFAuthenticationEntryPoint();
		}

		@Bean
		public LoginSuccessHandler authSuccess()
		{
			return new LoginSuccessHandler();
		}

		@Bean
		public LoginFailureHandler authFailure() {
			return new LoginFailureHandler();
		}
	}
	}