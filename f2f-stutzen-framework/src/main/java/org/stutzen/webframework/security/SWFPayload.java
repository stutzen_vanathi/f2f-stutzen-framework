package org.stutzen.webframework.security;

public class SWFPayload
{
	private String iss;
	private String typ;
	private String aud;
	private Long iat;
	private Long exp;
	private String userName;
	private int userId;

	public String getIss()
	{
		return this.iss;
	}

	public void setIss(String iss) {
		this.iss = iss;
	}

	public String getTyp() {
		return this.typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getAud() {
		return this.aud;
	}

	public void setAud(String aud) {
		this.aud = aud;
	}

	public Long getIat() {
		return this.iat;
	}

	public void setIat(Long iat) {
		this.iat = iat;
	}

	public Long getExp() {
		return this.exp;
	}

	public void setExp(Long exp) {
		this.exp = exp;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String toString()
	{
		return "Payload [iss=" + this.iss + ", typ=" + this.typ + ", aud=" + this.aud +
				", iat=" + this.iat + ", exp=" + this.exp + ", userName=" + this.userName + ",userId="+this.userId+"]";
	}
}