package org.stutzen.webframework.security;




import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;
import org.springframework.stereotype.Service;
import org.stutzen.webframework.security.CustomUser;

//NOT USED - DEVELOPMENT NOT COMPLETED
@Service
public class CustomUserDetailsService extends JdbcDaoImpl implements UserDetailsService {

@Override
public UserDetails loadUserByUsername(String username)
		throws UsernameNotFoundException {
	System.out.println("############common User name"+username);
	int userId = getJdbcTemplate().queryForObject("select userid from tbl_user where username=?",
			Integer.class, new Object[]{username});
	 List authorities = new ArrayList();
	 authorities.add(new SimpleGrantedAuthority(getAuthoritiesByUsernameQuery()));
	return new CustomUser(username,"protected",true,true,true,true,authorities,userId);
}

@Override
public UserDetails createUserDetails(final String username,
    final UserDetails userFromUserQuery,
    final List<GrantedAuthority> combinedAuthorities) {
  String returnUsername = userFromUserQuery.getUsername();


  final CustomUser userToReturn = new CustomUser(returnUsername,
      userFromUserQuery.getPassword(), userFromUserQuery.isEnabled(), true,
      true, true, combinedAuthorities,((CustomUser) userFromUserQuery).getUserId());
  return userToReturn;
}

}
