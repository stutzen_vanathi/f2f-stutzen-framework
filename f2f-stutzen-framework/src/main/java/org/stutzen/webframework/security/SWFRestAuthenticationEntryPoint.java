package org.stutzen.webframework.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.stutzen.webframework.logger.LogFormater;
import org.stutzen.webframework.logger.LoggerManager;

public class SWFRestAuthenticationEntryPoint
implements AuthenticationEntryPoint
{
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
			throws IOException, ServletException
	{
		LoggerManager.info(LogFormater.getLogMessage(getClass(), "commence", " contentType : " + request.getContentType()));
		response.sendError(401, "Unauthorized");
	}
}