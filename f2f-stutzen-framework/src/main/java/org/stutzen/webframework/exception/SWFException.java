package org.stutzen.webframework.exception;

@SuppressWarnings("serial")
public class SWFException extends Exception{

	public SWFException(String message) {
		super(message);
	}

}
