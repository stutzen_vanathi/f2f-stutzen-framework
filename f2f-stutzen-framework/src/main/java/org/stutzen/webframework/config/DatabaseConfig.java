package org.stutzen.webframework.config;

import java.beans.PropertyVetoException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.stutzen.webframework.constants.SWFConstants;
import org.stutzen.webframework.logger.LogFormater;
import org.stutzen.webframework.logger.LoggerManager;

import com.mchange.v2.c3p0.ComboPooledDataSource;

@Configuration
@EnableTransactionManagement
public class DatabaseConfig
{

	@Autowired
	private Environment env;

	@Bean(name={"dataSource"})
	public ComboPooledDataSource dataSource()
	{
		ComboPooledDataSource cpDataSource = new ComboPooledDataSource();
		try {
			cpDataSource.setDriverClass(this.env.getProperty("jdbc.driverClass", "com.mysql.jdbc.Driver"));
			cpDataSource.setJdbcUrl(this.env.getProperty("jdbc.jdbcUrl"));
			cpDataSource.setUser(this.env.getProperty("jdbc.user"));
			cpDataSource.setPassword(this.env.getProperty("jdbc.password"));
			cpDataSource.setMinPoolSize(((Integer)this.env.getProperty("jdbc.minPoolSize", Integer.class, SWFConstants.JDBC_POOL_MINSIZE)).intValue());
			cpDataSource.setMaxPoolSize(((Integer)this.env.getProperty("jdbc.maxPoolSize", Integer.class, SWFConstants.JDBC_POOL_MAXSIZE)).intValue());
			cpDataSource.setBreakAfterAcquireFailure(((Boolean)this.env.getProperty("jdbc.breakAfterAcquireFailure", Boolean.class, SWFConstants.JDBC_POOL_BREAKAFTERFAILURE)).booleanValue());
			cpDataSource.setAcquireRetryAttempts(((Integer)this.env.getProperty("jdbc.acquireRetryAttempts", Integer.class, SWFConstants.JDBC_POOL_RETRYATTEMPT)).intValue());
			cpDataSource.setIdleConnectionTestPeriod(((Integer)this.env.getProperty("jdbc.idleConnectionTestPeriod", Integer.class, SWFConstants.JDBC_POOL_IDLETESTPERIOD)).intValue());
			cpDataSource.setTestConnectionOnCheckout(((Boolean)this.env.getProperty("jdbc.testConnectionOnCheckout", Boolean.class, SWFConstants.JDBC_POOL_TESTONCHECKOUT)).booleanValue());
		} catch (IllegalStateException|PropertyVetoException e) {
			LoggerManager.error(LogFormater.getErrorLog(getClass(), "dataSource", "Error occurred while initailzing DataSource", e));
		}
		return cpDataSource;
	}

	@Bean
	public PlatformTransactionManager transactionManager() {
		return new DataSourceTransactionManager(dataSource());
	}
}