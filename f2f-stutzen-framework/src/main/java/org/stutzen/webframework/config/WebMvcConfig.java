package org.stutzen.webframework.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.stutzen.webframework.logger.LogFormater;
import org.stutzen.webframework.logger.LoggerManager;
import org.stutzen.webframework.security.SecurityConfig;

@Configuration
@EnableWebMvc
@EnableAspectJAutoProxy
@ComponentScan({"org.stutzen.webframework.controller", "org.stutzen.webframework.service", "co.stutzen"})
@PropertySources({@org.springframework.context.annotation.PropertySource({"classpath:/swf.properties"})})
@Import({SecurityConfig.class, DatabaseConfig.class, MyBatisConfig.class, ThymeleafConfig.class})
public class WebMvcConfig extends WebMvcConfigurerAdapter
{
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyPlaceHolderConfigurer()
	{
		return new PropertySourcesPlaceholderConfigurer();
	}

	public void addResourceHandlers(ResourceHandlerRegistry registry)
	{
		LoggerManager.debug(LogFormater.getLogMessage(getClass(), "addResourceHandlers", "setting up resource handlers"));
		registry.addResourceHandler(new String[] { "/**" }).addResourceLocations(new String[] { "/" });
	}
}