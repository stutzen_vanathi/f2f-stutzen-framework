package org.stutzen.webframework.controller;

import java.security.Principal;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.stutzen.webframework.util.SWFUtil;

@Controller
public class LoginController
{

	@Value("${security.success.url:/}")
	String successurl;

	@Value("${security.login.jsp:login}")
	String loginjsp;

	@RequestMapping(value={"${security.login.url:/login}"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	public String login(Principal p, HttpSession session)
	{
		if ((p == null) || (p.getName() == null)) {
			return this.loginjsp;
		}
		return SWFUtil.getRedirectURL(session.getServletContext().getContextPath(), this.successurl);
	}
}