
package org.stutzen.webframework.logger;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.stutzen.webframework.constants.SWFConstants;

/**
 * <p><b>Purpose</b>: Represents all log properties of the application
 * <p><b>Description</b>: Consists of static methods.
 * @
 */

public class LoggerManager {

	private static Logger logger = LogManager.getRootLogger();

	/**
	 * Method uses to call info method of slf4j
	 * @param Action Info
	 * @throws StutzenException
	 */
	public static void info(String  info) {
		logger.info(SWFConstants.LOG_SEPARATOR+info);

	}

	/**
	 * Method uses to call error method of slf4j
	 * @param userInfo
	 * @throws StutzenException
	 */
	public static void error(String error) {
		logger.error(SWFConstants.LOG_SEPARATOR+error);
	}

	/**
	 * Method uses to call debug method of slf4j
	 * @param userInfo
	 * @throws StutzenException
	 */
	public static void debug(String debug) {
		logger.debug(SWFConstants.LOG_SEPARATOR+debug);

	}  
}