package org.stutzen.webframework.logger;

import org.stutzen.webframework.util.SWFUtil;

public class LogFormater
{
	public static String getLogMessage(Class<?> className, String methodName, String message)
	{
		String logMessage = className + ">>" + methodName + "|" + message;
		return logMessage;
	}

	public static String getErrorMessage(Class<?> className, String methodName, String message) {
		String logMessage = className + ">>" + methodName + "|" + message;
		return logMessage;
	}

	public static String getErrorLog(Class<?> className, String methodName, String message, Throwable e) {
		String logMessage = className + ">>" + methodName + "|" + message + "|" + e.getMessage() + 
				"\n" + SWFUtil.stackTraceToString(e);
		return logMessage;
	}
}