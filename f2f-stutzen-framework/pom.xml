<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>org.stutzen.webframework</groupId>
  <artifactId>f2f-stutzen-framework</artifactId>
  <version>1.0.1</version>
  <packaging>jar</packaging>

<properties>

		<java-version>1.7</java-version>

		<servlet-api-version>3.1.0</servlet-api-version>

		<org.springframework-version>4.0.6.RELEASE</org.springframework-version>

		<org.springframework.security-version>3.2.5.RELEASE</org.springframework.security-version>

		<thymeleaf-version>2.1.2.RELEASE</thymeleaf-version>

		<org.aspectj-version>1.6.12</org.aspectj-version>

		<mybatis-version>3.2.2</mybatis-version>

		<mybatis-spring-version>1.2.2</mybatis-spring-version>

		<mybatis-ehcache-version>1.0.0</mybatis-ehcache-version>

		<ehcache-version>2.8.3</ehcache-version>

		<mysql.version>5.1.6</mysql.version>

		<c3p0.version>0.9.2.1</c3p0.version>

		<log4j.version>1.2.17</log4j.version>

		<jsontoken.version>1.1</jsontoken.version>

		<jackson-2-version>2.4.2</jackson-2-version>

		<json.version>20090211</json.version>

		<jsp-api-version>2.0</jsp-api-version>

		<taglib-version>1.1.2</taglib-version>

		<jstl-version>1.2</jstl-version>

		<commons-lang-version>2.5</commons-lang-version>

		<commons-codec-version>1.10</commons-codec-version>
	</properties>

	<dependencies>

		<!-- Spring -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-webmvc</artifactId>
			<version>${org.springframework-version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-jdbc</artifactId>
			<version>${org.springframework-version}</version>
		</dependency>

		<!-- Spring Security -->
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-web</artifactId>
			<version>${org.springframework.security-version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-config</artifactId>
			<version>${org.springframework.security-version}</version>
		</dependency>

		<!-- AspectJ -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-aop</artifactId>
			<version>${org.springframework-version}</version>
		</dependency>
		<dependency>
			<groupId>org.aspectj</groupId>
			<artifactId>aspectjrt</artifactId>
			<version>${org.aspectj-version}</version>
		</dependency>
		<dependency>
			<groupId>org.aspectj</groupId>
			<artifactId>aspectjweaver</artifactId>
			<version>${org.aspectj-version}</version>
		</dependency>

		<!-- MyBatis + EhCache -->
		<dependency>
			<groupId>org.mybatis</groupId>
			<artifactId>mybatis</artifactId>
			<version>${mybatis-version}</version>
		</dependency>
		<dependency>
			<groupId>org.mybatis</groupId>
			<artifactId>mybatis-spring</artifactId>
			<version>${mybatis-spring-version}</version>
		</dependency>
		<dependency>
			<groupId>org.mybatis</groupId>
			<artifactId>mybatis-ehcache</artifactId>
			<version>${mybatis-ehcache-version}</version>
			<exclusions>
				<exclusion>
					<artifactId>ehcache-core</artifactId>
					<groupId>net.sf.ehcache</groupId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>net.sf.ehcache</groupId>
			<artifactId>ehcache</artifactId>
			<version>${ehcache-version}</version>
		</dependency>

		<!-- MySQL database driver -->
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<version>${mysql.version}</version>
		</dependency>

		<!-- c3p0 connection pooling -->
		<dependency>
			<groupId>com.mchange</groupId>
			<artifactId>c3p0</artifactId>
			<version>${c3p0.version}</version>
		</dependency>

		<!-- Jackson JSON -->
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
			<version>${jackson-2-version}</version>
		</dependency>
		<dependency>
			<groupId>org.json</groupId>
			<artifactId>json</artifactId>
			<version>${json.version}</version>
		</dependency>

		<!-- Json Token -->
		<dependency>
			<groupId>com.googlecode.jsontoken</groupId>
			<artifactId>jsontoken</artifactId>
			<version>${jsontoken.version}</version>
			<exclusions>
				<exclusion>
					<artifactId>servlet-api</artifactId>
					<groupId>javax.servlet</groupId>
				</exclusion>
				<exclusion>
					<artifactId>commons-codec</artifactId>
					<groupId>commons-codec</groupId>
				</exclusion>
			</exclusions>
		</dependency>

		<!-- logentries -->
		<dependency>
			<groupId>log4j</groupId>
			<artifactId>log4j</artifactId>
			<version>${log4j.version}</version>
		</dependency>

		<dependency>
			<groupId>com.logentries</groupId>
			<artifactId>logentries-appender</artifactId>
			<version>RELEASE</version>
		</dependency>

		<!-- commons -->
		<dependency>
			<groupId>commons-lang</groupId>
			<artifactId>commons-lang</artifactId>
			<version>${commons-lang-version}</version>
		</dependency>

		<!-- Servlet -->
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>javax.servlet-api</artifactId>
			<version>${servlet-api-version}</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.thymeleaf</groupId>
			<artifactId>thymeleaf-spring4</artifactId>
			<version>${thymeleaf-version}</version>
		</dependency>
		<dependency>
			<groupId>org.thymeleaf.extras</groupId>
			<artifactId>thymeleaf-extras-springsecurity3</artifactId>
			<version>2.1.1.RELEASE</version>
		</dependency>
		<dependency>
			<groupId>net.sourceforge.nekohtml</groupId>
			<artifactId>nekohtml</artifactId>
			<version>1.9.15</version>
		</dependency>
		<!-- commons-codec -->
		<dependency>
			<groupId>commons-codec</groupId>
			<artifactId>commons-codec</artifactId>
			<version>${commons-codec-version}</version>

		</dependency>
	</dependencies>
	<build>
		<extensions>
			<extension>
				<groupId>org.kuali.maven.wagons</groupId>
				<artifactId>maven-s3-wagon</artifactId>
				<version>1.1.9</version>
			</extension>
		</extensions>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-compiler-plugin</artifactId>
					<version>3.1</version>
					<configuration>
						<source>${java-version}</source>
						<target>${java-version}</target>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>
	<distributionManagement>
		<site>
			<id>s3.site</id>
			<url>s3://co.stutzen.service/site</url>
		</site>
		<repository>
			<id>s3.release</id>
			<url>s3://co.stutzen.service/release</url>
		</repository>
		<snapshotRepository>
			<id>s3.snapshot</id>
			<url>s3://co.stutzen.service/snapshot</url>
		</snapshotRepository>
	</distributionManagement>
</project>